package me.smourad.LeagueOfAchievementBot.command;

import discord4j.core.GatewayDiscordClient;
import me.smourad.LeagueOfAchievementBot.league.LeagueRequestBuilder;
import me.smourad.LeagueOfAchievementBot.repository.LeagueAccountRepository;
import me.smourad.LeagueOfAchievementBot.repository.ProfileRepository;

public abstract class LeagueCommand extends DiscordCommand {

    protected final LeagueRequestBuilder leagueRequestBuilder;
    protected final ProfileRepository profileRepository;
    protected final LeagueAccountRepository leagueAccountRepository;

    public LeagueCommand(
            GatewayDiscordClient client,
            LeagueRequestBuilder leagueRequestBuilder,
            ProfileRepository profileRepository,
            LeagueAccountRepository leagueAccountRepository
    ) {
        super(client);
        this.leagueRequestBuilder = leagueRequestBuilder;
        this.profileRepository = profileRepository;
        this.leagueAccountRepository = leagueAccountRepository;

    }

}
