package me.smourad.LeagueOfAchievementBot.league;

public enum LeagueServer {

    BR("br1", "americas"),
    EUNE("eun1", "europe"),
    EUW("euw1", "europe"),
    JP("jp1", "asia"),
    KR("kr", "asia"),
    LAN("la1", "americas"),
    LAS("la2", "americas"),
    NA("na1", "americas"),
    OCE("oc1", "sea"),
    TR("tr1", "europe"),
    RU("ru", "europe");

    private final String id;
    private final String region;

    LeagueServer(String id, String region) {
        this.id = id;
        this.region = region;
    }

    public String getId() {
        return id;
    }

    public String getRegion() {
        return region;
    }

}
