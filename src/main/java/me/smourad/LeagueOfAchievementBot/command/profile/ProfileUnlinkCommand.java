package me.smourad.LeagueOfAchievementBot.command.profile;

import com.google.gson.JsonObject;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.spec.EmbedCreateSpec;
import me.smourad.LeagueOfAchievementBot.entity.LeagueAccount;
import me.smourad.LeagueOfAchievementBot.entity.Profile;
import me.smourad.LeagueOfAchievementBot.league.LeagueRequestBuilder;
import me.smourad.LeagueOfAchievementBot.league.LeagueServer;
import me.smourad.LeagueOfAchievementBot.repository.LeagueAccountRepository;
import me.smourad.LeagueOfAchievementBot.repository.ProfileRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

@Service
public class ProfileUnlinkCommand extends ProfileModifyLinkCommand {

    public ProfileUnlinkCommand(GatewayDiscordClient client, LeagueRequestBuilder leagueRequestBuilder, ProfileRepository profileRepository, LeagueAccountRepository leagueAccountRepository) {
        super(client, leagueRequestBuilder, profileRepository, leagueAccountRepository);
    }

    @Override
    public String getName() {
        return "unlink";
    }

    @Override
    protected String getDescription() {
        return "Unlink your LoL account from your Discord account";
    }

    @Override
    public Publisher<Void> execute(ChatInputInteractionEvent event) {
        BigInteger id = event.getInteraction().getUser().getId().asBigInteger();
        LeagueServer server = LeagueServer.valueOf(getStringParameter(event, SERVER));
        String username = getStringParameter(event, USERNAME);

        try {
            JsonObject account = leagueRequestBuilder.request(server.getId(),
                    "/lol/summoner/v4/summoners/by-name/%s".formatted(username));
            String puuid = account.get("puuid").getAsString();

            Profile profile = getProfile(id);
            List<LeagueAccount> leagueAccounts = leagueAccountRepository.findByProfile(profile);

            LeagueAccount leagueAccount = leagueAccounts.stream()
                    .filter(a -> a.getPuuid().equals(puuid))
                    .findFirst().orElse(null);

            EmbedCreateSpec.Builder embed = EmbedCreateSpec.builder();

            if (leagueAccount == null) {
                embed.title("This account is not linked to your Discord account");
            } else {
                leagueAccountRepository.delete(leagueAccount);
                embed.title("Bye Bye " + account.get("name").getAsString());
            }

            profileRepository.save(profile);
            return event.reply().withEmbeds(embed.build());
        } catch (IOException e) {
            e.printStackTrace();
            return event.reply(e.getMessage());
        }
    }
}
