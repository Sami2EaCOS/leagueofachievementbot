package me.smourad.LeagueOfAchievementBot.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
public class Profile {

    @Id
    private BigInteger id;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LeagueAccount> accounts;

}
