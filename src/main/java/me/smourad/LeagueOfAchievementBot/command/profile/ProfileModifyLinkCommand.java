package me.smourad.LeagueOfAchievementBot.command.profile;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.command.ApplicationCommandOption;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import me.smourad.LeagueOfAchievementBot.command.LeagueCommand;
import me.smourad.LeagueOfAchievementBot.entity.Profile;
import me.smourad.LeagueOfAchievementBot.league.LeagueRequestBuilder;
import me.smourad.LeagueOfAchievementBot.repository.LeagueAccountRepository;
import me.smourad.LeagueOfAchievementBot.repository.ProfileRepository;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

public abstract class ProfileModifyLinkCommand extends LeagueCommand {

    protected final String SERVER = "server";
    protected final String USERNAME = "username";

    public ProfileModifyLinkCommand(GatewayDiscordClient client, LeagueRequestBuilder leagueRequestBuilder, ProfileRepository profileRepository, LeagueAccountRepository leagueAccountRepository) {
        super(client, leagueRequestBuilder, profileRepository, leagueAccountRepository);
    }

    @Override
    protected List<ApplicationCommandOptionData> getOptions() {
        return List.of(
                ApplicationCommandOptionData.builder()
                        .name(SERVER)
                        .description("Your LoL account Server")
                        .type(ApplicationCommandOption.Type.STRING.getValue())
                        .required(true)
                        .build(),
                ApplicationCommandOptionData.builder()
                        .name(USERNAME)
                        .description("Your LoL account Username")
                        .type(ApplicationCommandOption.Type.STRING.getValue())
                        .required(true)
                        .build()
        );
    }

    protected Profile getProfile(BigInteger id) {
        return profileRepository.findById(id)
                .orElse(new Profile().setId(id).setAccounts(Collections.emptyList()));
    }

}
