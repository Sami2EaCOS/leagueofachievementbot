package me.smourad.LeagueOfAchievementBot.entity;

import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;
import me.smourad.LeagueOfAchievementBot.league.LeagueServer;

@Entity
@Data
@Accessors(chain = true)
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "puuid", "profile_id" }) })
public class LeagueAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Nonnull
    @Enumerated(EnumType.STRING)
    private LeagueServer server;

    @Nonnull
    private String puuid;

    @Nonnull
    private String summonerId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Profile profile;

}
