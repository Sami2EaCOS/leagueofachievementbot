package me.smourad.LeagueOfAchievementBot.command;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DiscordCommandListener {

    private final GatewayDiscordClient client;

    public DiscordCommandListener(GatewayDiscordClient client, List<DiscordCommand> commands) {
        this.client = client;
        listen(commands);
    }

    protected void listen(List<DiscordCommand> commands) {
        Map<String, DiscordCommand> commandsMapping = commands.stream()
                .collect(Collectors.toMap(DiscordCommand::getName, c -> c));

        client.on(ChatInputInteractionEvent.class, event -> {
            String commandName = event.getCommandName();

            if (commandsMapping.containsKey(commandName)) {
                return commandsMapping.get(commandName).execute(event);
            }

            return null;
        }).subscribe();
    }
}
