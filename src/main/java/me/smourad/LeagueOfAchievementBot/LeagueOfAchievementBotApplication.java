package me.smourad.LeagueOfAchievementBot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeagueOfAchievementBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeagueOfAchievementBotApplication.class, args);
	}

}
