package me.smourad.LeagueOfAchievementBot.repository;

import me.smourad.LeagueOfAchievementBot.entity.LeagueAccount;
import me.smourad.LeagueOfAchievementBot.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LeagueAccountRepository extends JpaRepository<LeagueAccount, Long> {

    Optional<LeagueAccount> findByProfileAndPuuid(Profile profile, String puuid);

    List<LeagueAccount> findByProfile(Profile profile);

}
