package me.smourad.LeagueOfAchievementBot.command.profile;

import com.google.gson.JsonObject;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandOption;
import discord4j.core.spec.EmbedCreateFields;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import me.smourad.LeagueOfAchievementBot.command.LeagueCommand;
import me.smourad.LeagueOfAchievementBot.entity.LeagueAccount;
import me.smourad.LeagueOfAchievementBot.entity.Profile;
import me.smourad.LeagueOfAchievementBot.league.LeagueRequestBuilder;
import me.smourad.LeagueOfAchievementBot.league.LeagueServer;
import me.smourad.LeagueOfAchievementBot.repository.LeagueAccountRepository;
import me.smourad.LeagueOfAchievementBot.repository.ProfileRepository;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProfileLinkCommand extends ProfileModifyLinkCommand {

    public ProfileLinkCommand(
            GatewayDiscordClient client,
            LeagueRequestBuilder leagueRequestBuilder,
            ProfileRepository profileRepository,
            LeagueAccountRepository leagueAccountRepository
    ) {
        super(client, leagueRequestBuilder, profileRepository, leagueAccountRepository);
    }

    @Override
    public String getName() {
        return "link";
    }

    @Override
    protected String getDescription() {
        return "Link your LoL account to your Discord account";
    }

    @Override
    public Publisher<Void> execute(ChatInputInteractionEvent event) {
        BigInteger id = event.getInteraction().getUser().getId().asBigInteger();
        LeagueServer server = LeagueServer.valueOf(getStringParameter(event, SERVER));
        String username = getStringParameter(event, USERNAME);

        try {
            JsonObject account = leagueRequestBuilder.request(server.getId(),
                    "/lol/summoner/v4/summoners/by-name/%s".formatted(username));
            String puuid = account.get("puuid").getAsString();

            Profile profile = getProfile(id);
            List<LeagueAccount> leagueAccounts = leagueAccountRepository.findByProfile(profile);

            LeagueAccount leagueAccount = leagueAccounts.stream()
                    .filter(a -> a.getPuuid().equals(puuid))
                    .findFirst().orElse(null);

            EmbedCreateSpec.Builder embed = EmbedCreateSpec.builder();

            if (leagueAccount == null) {
                String summonerId = account.get("id").getAsString();
                leagueAccount = generateLeagueAccount(profile, server, puuid, summonerId);
                List<LeagueAccount> accounts = new ArrayList<>(profile.getAccounts());
                accounts.add(leagueAccount);
                profile.setAccounts(accounts);

                embed  .title("Account " + account.get("name").getAsString() + " linked to your Discord account")
                       .image("http://ddragon.leagueoflegends.com/cdn/12.22.1/img/profileicon/"
                                + account.get("profileIconId").getAsLong() + ".png")
                       .addField(EmbedCreateFields.Field.of("Level",
                               account.get("summonerLevel").getAsString(), true));
            } else {
                embed.title("This account is already linked to your Discord account");
            }

            profileRepository.save(profile);
            return event.reply().withEmbeds(embed.build());
        } catch (IOException e) {
            e.printStackTrace();
            return event.reply(e.getMessage());
        }
    }

    private LeagueAccount generateLeagueAccount(
            Profile profile, LeagueServer server, String puuid, String summonerId) {
        return new LeagueAccount()
                .setProfile(profile)
                .setServer(server)
                .setPuuid(puuid)
                .setSummonerId(summonerId);
    }

}
