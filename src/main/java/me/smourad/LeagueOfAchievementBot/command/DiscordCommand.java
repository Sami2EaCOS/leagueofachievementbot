package me.smourad.LeagueOfAchievementBot.command;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import discord4j.discordjson.json.ApplicationCommandRequest;
import org.reactivestreams.Publisher;

import java.util.List;
import java.util.function.Function;

public abstract class DiscordCommand {

    protected final GatewayDiscordClient client;

    public DiscordCommand(GatewayDiscordClient client) {
        this.client = client;
        registerCommand();
    }

    public abstract String getName();
    protected abstract String getDescription();

    protected abstract List<ApplicationCommandOptionData> getOptions();

    protected void registerCommand() {
        Long applicationId = client.getRestClient().getApplicationId().block();

        ApplicationCommandRequest commandRequest = ApplicationCommandRequest.builder()
                .name(getName())
                .description(getDescription())
                .addAllOptions(getOptions())
                .build();

        client.getRestClient().getApplicationService()
                .createGlobalApplicationCommand(applicationId, commandRequest)
                .subscribe();
    }

    public abstract Publisher<Void> execute(ChatInputInteractionEvent event);

    private Object getParameter(ChatInputInteractionEvent event, String key, Function<? super ApplicationCommandInteractionOptionValue, ?> type) {
        return event.getOption(key)
                .flatMap(ApplicationCommandInteractionOption::getValue)
                .map(type)
                .orElse(null);
    }

    protected String getStringParameter(ChatInputInteractionEvent event, String key) {
        return (String) getParameter(event, key, ApplicationCommandInteractionOptionValue::asString);
    }

    protected Long getLongParameter(ChatInputInteractionEvent event, String key) {
        return (Long) getParameter(event, key, ApplicationCommandInteractionOptionValue::asLong);
    }
}
