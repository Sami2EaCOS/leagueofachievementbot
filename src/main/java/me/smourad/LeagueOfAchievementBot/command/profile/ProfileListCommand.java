package me.smourad.LeagueOfAchievementBot.command.profile;

import com.google.gson.JsonObject;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.spec.EmbedCreateFields;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import me.smourad.LeagueOfAchievementBot.command.LeagueCommand;
import me.smourad.LeagueOfAchievementBot.entity.LeagueAccount;
import me.smourad.LeagueOfAchievementBot.entity.Profile;
import me.smourad.LeagueOfAchievementBot.league.LeagueRequestBuilder;
import me.smourad.LeagueOfAchievementBot.repository.LeagueAccountRepository;
import me.smourad.LeagueOfAchievementBot.repository.ProfileRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfileListCommand extends LeagueCommand {

    public ProfileListCommand(
            GatewayDiscordClient client,
            LeagueRequestBuilder leagueRequestBuilder,
            ProfileRepository profileRepository,
            LeagueAccountRepository leagueAccountRepository
    ) {
        super(client, leagueRequestBuilder, profileRepository, leagueAccountRepository);
    }

    @Override
    public String getName() {
        return "list";
    }

    @Override
    protected String getDescription() {
        return "List all LoL account linked to your Discord account";
    }

    @Override
    protected List<ApplicationCommandOptionData> getOptions() {
        return Collections.emptyList();
    }

    @Override
    public Publisher<Void> execute(ChatInputInteractionEvent event) {
        BigInteger id = event.getInteraction().getUser().getId().asBigInteger();

        if (profileRepository.existsById(id)) {
            Profile profile = profileRepository.findById(id).get();
            List<LeagueAccount> accounts = profile.getAccounts();

            if (accounts.size() > 0) {
                List<EmbedCreateSpec> embeds = accounts.stream()
                        .map(account -> {
                            EmbedCreateSpec.Builder embed = EmbedCreateSpec.builder();

                            try {
                                JsonObject info = leagueRequestBuilder.request(account.getServer().getId(),
                                        "/lol/summoner/v4/summoners/by-puuid/%s".formatted(account.getPuuid()));

                                embed.title(info.get("name").getAsString())
                                        .image("http://ddragon.leagueoflegends.com/cdn/12.22.1/img/profileicon/" +
                                                info.get("profileIconId").getAsLong() + ".png")
                                        .addField(EmbedCreateFields.Field.of("Level",
                                                info.get("summonerLevel").getAsString(), true));
                            } catch (IOException e) {
                                e.printStackTrace();
                                embed.title("Error for this account (puuid: %s)".formatted(account.getPuuid()));
                            }

                            return embed.build();
                        })
                        .collect(Collectors.toList());

                return event.reply().withEmbeds(embeds);
            }
        }

        EmbedCreateSpec embed = EmbedCreateSpec.builder()
                .title("You need to link LoL accounts with /link")
                .build();

        return event.reply().withEmbeds(embed);
    }

}
