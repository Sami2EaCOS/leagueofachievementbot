package me.smourad.LeagueOfAchievementBot.league;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

@Service
public class LeagueRequestBuilder {

    public JsonObject request(String host, String routes) throws IOException {
        URL url = new URL("https://" + host + ".api.riotgames.com" + routes);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("X-Riot-Token", System.getenv("RIOT_TOKEN"));
        connection.connect();

        StringBuilder json = new StringBuilder();
        Scanner scanner = new Scanner(connection.getInputStream());

        while (scanner.hasNext()) {
            json.append(scanner.nextLine());
        }

        scanner.close();

        JsonObject jsonObject = JsonParser.parseString(json.toString()).getAsJsonObject();

        int responseCode = connection.getResponseCode();

        if (responseCode != 200) {
            JsonObject statusObject = jsonObject.get("status").getAsJsonObject();
            int code = statusObject.get("code").getAsInt();
            String message = statusObject.get("message").getAsString();

            throw new RuntimeException(code + ": " + message);
        } else {
            return jsonObject;
        }
    }

}
